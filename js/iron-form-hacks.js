/**
 * @file
 * iron-form currently submits forms via ajax. This hack brings the old-page-redirect-submit back. See https://github.com/PolymerElements/iron-form/issues/105#issuecomment-243545532.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';

  var ironForm = document.querySelector('form[is=iron-form]');

  if (ironForm) {
    ironForm.addEventListener('iron-form-presubmit', function(event) {
      var vals = ironForm.serialize();
      var form, hiddenField;

      event.preventDefault();

      // Create new form
      form = document.createElement('form');

      form.action = ironForm.action;
      form.method = ironForm.method;
      form.style.display = 'none';

      // Create hidden fields in form
      for (var key in vals) {
        if (vals.hasOwnProperty(key)) {
          hiddenField = document.createElement('input');

          hiddenField.type = 'hidden';
          hiddenField.name = key;
          hiddenField.value = vals[key];

          form.appendChild(hiddenField);
        }
      }

      // Add form to DOM and submit it
      document.body.appendChild(form);
      form.submit();
    });
  }

})(jQuery, Drupal, drupalSettings);